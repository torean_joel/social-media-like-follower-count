<!doctype html>
<html>
	<head>
		<title>Social Media Widget</title>
		<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
		<style>

		/*import font*/
		@import url(http://fonts.googleapis.com/css?family=Poiret+One);

		/*media queries*/
		@media all and (max-width: 895px) {
			#container {
				display: none !important;
			}
		}
		@media all and (min-width: 992px) and (max-width: 1110px) {
			#container {
				display: none !important;
			}
		}
		@media all and (min-width: 1200px) and (max-width: 1315px) {
			#container {
				display: none !important;
			}
		}

		#container {
			position: fixed;
			top: 30%;
			padding: 5px;
			z-index: 1000;
		}

		body {
			background: #000;
			margin: 0 auto;
			padding: auto;
		}
		/*buttons*/
		#facebook {
			background: #3B5A9A;
			/* height: 45px; */
			width: 50px;
			/* margin: 5px; */
			padding: 4px;
		}
		#twitter {
			background: #29A9E1;
			/* height: 45px; */
			width: 50px;
			/* margin: 5px; */
			padding: 4px;
		}
		#google {
			background: #DF4B38;
			/* height: 45px; */
			width: 50px;
			/* margin: 5px; */
			padding: 4px;
		}

		/*The icons*/
		.facebook_icon {
			/*width: 30px;*/
			height: 45px;
			background: url('social.png');
			background-position: -37px -30px;
			/*float: left;*/
			padding-right: 5px;
		}
		.twitter_icon {
			/*width: 30px;*/
			height: 45px;
			background: url('social.png');
			background-position: -96px -32px;
			/*float: left;*/
			padding-right: 5px;
		}
		.google_icon {
			/*width: 30px;*/
			height: 45px;
			background: url('social.png');
			background-position: -216px -32px;
			/*float: left;*/
			padding-right: 5px;
		}

		/*data*/

		#link {
			text-decoration: none;
		}

		/*data*/
		#link .data {
			font-size: 22px;
			text-align: center;
			color: #fff;
			padding-right: 5px;
			font-family: 'Poiret One', cursive;
			font-weight: 600;
			/*padding-top: 10px;*/
		}

		</style>
	</head>
	<body>
		<!--the buttons that will hold the valies of the likes and the liking of the pages-->
		<div id="container">
			
				<a id="link" href="" target="_blank"> <!--Link to your facebook page - a normal tag-->
					<div id="facebook">
					<!--<img alt="facebook" src=""/>-->
					<div class="facebook_icon"></div>
					<div class="data" id="fb_count"></div>
					</div>
				</a>
			
			    <!--php code for twitter-->
	              <?php
                        /*ini_set('display_errors', true);*/
                        /* Lines 13-53 gets twitter followers */
                        // Require the library file, obviously
                        require_once('TwitterAPIExchange.php');

                        // Set up your settings with the keys you get from the dev site
                        $settings = array(
                                    'oauth_access_token' => "",
                                    'oauth_access_token_secret' => "",
                                    'consumer_key' => "",
                                    'consumer_secret' => ""
                        );

                        // Chooose the url you want from the docs, this is the users/show
            
                        $url = "https://api.twitter.com/1.1/statuses/user_timeline.json";

                        // The request method, according to the docs, is GET, not POST
                        $requestMethod = 'GET';

                        // Set up your get string, we're using my screen name here
                        $getfield = ""; //?screen_name=TWITTERHANDLEHERE

                        // Create the object
                                    $twitter = new TwitterAPIExchange($settings);

                                    $string = json_decode(
                                        $twitter->setGetfield($getfield)
                                            ->buildOauth($url, $requestMethod)
                                            ->performRequest(), $assoc = true
                                    );
                        /** Checks for errors in string and displays them should they happen **/
                        if (isset($string['errors'][0]["message"])) {
                                        echo "<h3>Sorry, there was a problem.</h3><p>Twitter returned the following
                                    error message:</p><p><em>".$string[ERRORS][0]['message']."</em></p>";exit();
                        } else {


                            $var1 = $string[0]["user"];
                            $var2 = $var1["followers_count"];
                            //echo $var2 ;

                            echo '<a id="link" href="#" target="_blank">'; //Link to your twitter page - a normal tag
					        echo '<div id="twitter">';
					        //<!--<img alt="twitter" src=""/>-->
					        echo '<div class="twitter_icon"></div>';
					        echo '<div class="data" id="twitter_count">'.$var2.'</div>';
					        echo '</div>';
				            echo '</a>';
                        }
                    ?>
				

				<a id="link" href="" target="_blank"> <!--Link to your google + page - a normal tag-->
					<div id="google">
					<!--<img alt="facebook" src=""/>-->
					<div class="google_icon"></div>
					<div class="data" id="google_count"></div>
					</div>
				</a>
			
		</div>	
	</body>
	<script type="text/javascript">
        //facebook
        $.getJSON( "https://api.facebook.com/method/fql.query?query=select%20like_count%20from%20link_stat%20where%20url=%27https://www.facebook.com/FACEBOOKPAGENAMEHERE%27&format=json", 
        	//https://api.facebook.com/method/fql.query?query=select%20like_count%20from%20link_stat%20where%20url=%27https://www.facebook.com/premierleague%27&format=json - 
        	//example, note the permier league name in url, you can paste this in your browser to view the json object with the like count properties
                  function( jsonFacebook ) {
                    var num = jsonFacebook[0].like_count;
                    var arr = num.toString().split("").map(function(i){return parseInt(i)});
                    if(arr.length === 4) {
                        arr.reverse().splice(0,3);
                        arr.reverse();
                        $("#fb_count").append(arr.join("") + "K");
                    } else if(arr.length === 5) {
                        arr.reverse().splice(0,3);
                        arr.reverse();
                        $("#fb_count").append(arr.join("") + "K");
                    } else if(arr.length === 6) {
                        arr.reverse().splice(0,3);
                        arr.reverse();
                        $("#fb_count").append(arr.join("") + "K");
                    } else if(arr.length === 7) {
                        arr.reverse().splice(0,6);
                        arr.reverse();
                        $("#fb_count").append(arr.join("") + "M");
                    } else {
                       $("#fb_count").append(arr.join(""));
                    }
                });


        //google plus
        $.getJSON("https://www.googleapis.com/plus/v1/people/GooglePlusID?key=AIzaSyBqpsi3IBh84aN8yeBHghIniYXqbRDXHkk", 
        	//https://www.googleapis.com/plus/v1/people/117252595282750343993?key=AIzaSyBqpsi3IBh84aN8yeBHghIniYXqbRDXHkk - this is a link with my google plus page - note the id - 
        	//paste this into the browser to make sure it works and you can see the json object it returs that the code below uses to get the relevant properties
                  function( jsonGooglePlus ) {
                    var num = jsonGooglePlus.circledByCount;
                    var arr = num.toString().split("").map(function(i){return parseInt(i)});
                    if(arr.length === 4) {
                        arr.reverse().splice(0,3);
                        arr.reverse();
                        $("#google_count").append(arr.join("") + "K");
                    } else if(arr.length === 5) {
                        arr.reverse().splice(0,3);
                        arr.reverse();
                        $("#google_count").append(arr.join("") + "K");
                    } else if(arr.length === 6) {
                        arr.reverse().splice(0,3);
                        arr.reverse();
                        $("#google_count").append(arr.join("") + "K");
                    } else if(arr.length === 7) {
                        arr.reverse().splice(0,6);
                        arr.reverse();
                        $("#google_count").append(arr.join("") + "M");
                    } else {
                        $("#google_count").append(arr.join(""));
                    }
        })
	</script>

</html>
