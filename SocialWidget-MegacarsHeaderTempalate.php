<?php
/* 
* Theme: PREMIUMPRESS CORE FRAMEWORK FILE
* Url: www.premiumpress.com
* Author: Mark Fail
*
* THIS FILE WILL BE UPDATED WITH EVERY UPDATE
* IF YOU WANT TO MODIFY THIS FILE, CREATE A CHILD THEME
*
* http://codex.wordpress.org/Child_Themes
*/
if (!defined('THEME_VERSION')) {	header('HTTP/1.0 403 Forbidden'); exit; }// Turn off error reportingerror_reporting(0);// Report runtime errorserror_reporting(E_ERROR | E_WARNING | E_PARSE);
?>

<?php global $CORE, $OBJECTS, $userdata; ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<!--[if lte IE 8 ]><html lang="en" class="ie ie8"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie"><![endif]-->
<head>
<meta name="google-site-verification" content="A916lTqIdZfD7hf-po7kTUYcUi5lHLcj8h6I0w5JqRU" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge" /><![endif]-->

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title> 
<?php wp_head(); ?> 
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	
<![endif]-->
<style>

		/*import font*/
		@import url(http://fonts.googleapis.com/css?family=Poiret+One);

		/*media queries*/
		@media all and (max-width: 895px) {
			#container {
				display: none !important;
			}
		}
		@media all and (min-width: 992px) and (max-width: 1110px) {
			#container {
				display: none !important;
			}
		}
		@media all and (min-width: 1200px) and (max-width: 1315px) {
			#container {
				display: none !important;
			}
		}
		#container {
			position: fixed;
			top: 30%;
			padding: 5px;
			z-index: 1000;
		}

		body {
			background: #000;
			margin: 0 auto;
			padding: auto;
		}
		/*buttons*/
		#facebook {
			background: #3B5A9A;
			/* height: 45px; */
			width: 50px;
			/* margin: 5px; */
			padding: 4px;
		}
		#twitter {
			background: #29A9E1;
			/* height: 45px; */
			width: 50px;
			/* margin: 5px; */
			padding: 4px;
		}
		#google {
			background: #DF4B38;
			/* height: 45px; */
			width: 50px;
			/* margin: 5px; */
			padding: 4px;
		}


		/*The icons*/
		.facebook_icon {
			width: 40px;
			height: 45px;
			background: url('http://megacars.co.za/wp-content/uploads/2015/01/social.png');
			background-position: -37px -30px;
			float: left;
			padding-right: 5px;
		}
		.twitter_icon {
			width: 40px;
			height: 45px;
			background: url('http://megacars.co.za/wp-content/uploads/2015/01/social.png');
			background-position: -96px -32px;
			float: left;
			padding-right: 5px;
		}
		.google_icon {
			width: 40px;
			height: 45px;
			background: url('http://megacars.co.za/wp-content/uploads/2015/01/social.png');
			background-position: -216px -32px;
			float: left;
			padding-right: 5px;
		}

		/*data*/

		#link {
			text-decoration: none;
		}

		/*data*/
		#link .data {
			font-size: 18px;
			text-align: right;
			color: #fff;
			padding-right: 5px;
			font-family: 'Poiret One', cursive;
			font-weight: 600;
			padding-top: 10px;
		}

</style>


<?php wp_enqueue_script("jquery"); ?>
<script type="text/javascript"
   src="<?php bloginfo("template_url"); ?>/js/jquery.min.js"></script>

</head>

 
<body <?php body_class(); ?> <?php echo $CORE->ITEMSCOPE('webpage'); ?>>

<!--the code for the social widget, beta - will be made into plugin soon-->

<!--the buttons that will hold the valies of the likes and the liking of the pages-->
		<div id="container">
			
				<a id="link" href="https://www.facebook.com/MegaCarsSouthAfrica" target="_blank">
					<div id="facebook">
					<!--<img alt="facebook" src=""/>-->
					<div class="facebook_icon"></div>
					<div class="data" id="fb_count"></div>
					</div>
				</a>
			
			    <!--php code for twitter-->
	              <?php
                        /*ini_set('display_errors', true);*/
                        /* Lines 13-53 gets twitter followers */
                        // Require the library file, obviously
                        require_once('TwitterAPIExchange.php');

                        // Set up your settings with the keys you get from the dev site
                        $settings = array(
                                    'oauth_access_token' => "2976689500-i03nxhd6hlypHALizi66FVthXMUxIbOnQPunLME",
                                    'oauth_access_token_secret' => "awrnZJpzTAH7ihokabIShuvY5KCxTZ2fwSLTiZh63v4Vu",
                                    'consumer_key' => "ibxXG70WSY86Ng470xv2Ek9VS",
                                    'consumer_secret' => "Uy2FuNzDfnygYSgFHd4tXrFjRyVM4gESz6kdxOzmBigTEUgWmR"
                        );

                        // Chooose the url you want from the docs, this is the users/show
            
                        $url = "https://api.twitter.com/1.1/statuses/user_timeline.json";

                        // The request method, according to the docs, is GET, not POST
                        $requestMethod = 'GET';

                        // Set up your get string, we're using my screen name here
                        $getfield = "?screen_name=MegaCarsSA";

                        // Create the object
                                    $twitter = new TwitterAPIExchange($settings);

                                    $string = json_decode(
                                        $twitter->setGetfield($getfield)
                                            ->buildOauth($url, $requestMethod)
                                            ->performRequest(), $assoc = true
                                    );
                        /** Checks for errors in string and displays them should they happen **/
                        if (isset($string['errors'][0]["message"])) {
                                        echo "<h3>Sorry, there was a problem.</h3><p>Twitter returned the following
                                    error message:</p><p><em>".$string[ERRORS][0]['message']."</em></p>";exit();
                        } else {


                            $var1 = $string[0]["user"];
                            $var2 = $var1["followers_count"];
                            //echo $var2 ;

                            echo '<a id="link" href="https://www.twitter.com/MegaCarsSA" target="_blank">';
					        echo '<div id="twitter">';
					        //<!--<img alt="twitter" src=""/>-->
					        echo '<div class="twitter_icon"></div>';
					        echo '<div class="data" id="twitter_count">'.$var2.'</div>';
					        echo '</div>';
				            echo '</a>';
                        }
                    ?>
				

				<a id="link" href="https://plus.google.com/118346766221260813565/posts" target="_blank">
					<div id="google">
					<!--<img alt="facebook" src=""/>-->
					<div class="google_icon"></div>
					<div class="data" id="google_count"></div>
					</div>
				</a>
			
		</div>	


<!--scripts-->
<!--<script src="http://www.megacars.co.za/wp-content/themes/DL/jquery.min.js"></script>-->
	<script type="text/javascript">
		var f_page = "MegaCarsSouthAfrica";
	
jQuery(function($) {

		function add_commas(number) {
			if (number.length > 3) {
				var mod = number.length % 3;
				var output = (mod > 0 ? (number.substring(0,mod)) : '');
				for (i=0 ; i < Math.floor(number.length / 3); i++) {
					if ((mod == 0) && (i == 0)) {
						output += number.substring(mod+ 3 * i, mod + 3 * i + 3);
					} else {
						output+= ',' + number.substring(mod + 3 * i, mod + 3 * i + 3);	
					}
				}
				return (output);
			} else {
				return number;
			}
		}
		
		// when document is ready load the counts
		$(document).ready(function(){
		
			// grab from facebook
			$.getJSON('https://graph.facebook.com/'+f_page+'?callback=?', function(data) {
				var fb_count = data['likes'].toString();
				fb_count = add_commas(fb_count);
				$('#fb_count').html(fb_count);
			});			
		});

});
	</script>

	<!--javascript code for google plus-->
	<script type="text/javascript">

jQuery(function($) {

	var profileid = '118346766221260813565';
	var apikey = 'AIzaSyCaIc9zu1f1sg_lne0xWMydzrcoQQ5yZYg';
	var url = 'https://www.googleapis.com/plus/v1/people/' + profileid + '?key=' + apikey;
	$.ajax({
	    type: "GET",
	    dataType: "json",
	    url: url,
	    success: function (data) {
	        var googlefollowcount = data.circledByCount;
	        $("#google_count").html(googlefollowcount);
	    }
	});

});
</script>

<!-- end of code for beta social feeder-->
 
<div class="page-wrapper <?php $CORE->CSS("mode"); ?>" id="<?php echo THEME_TAXONOMY; ?>_styles">
 
<?php hook_wrapper_before(); ?>  
     
<div class="header_wrapper">

    <header id="header">
    
        <div class="overlay"> 
    
        <?php echo hook_topmenu(_design_topmenu()).hook_header(_design_header()).hook_menu(_design_menu(),1); ?>
    
        <?php hook_container_before(); ?>
    
        </div>
    
    </header>

	<?php hook_header_after(); ?>
 
</div> 

<div id="core_padding">

	<div class="<?php $CORE->CSS("container"); ?><?php $CORE->CSS("2columns"); ?> core_section_top_container">
    
    <?php echo $CORE->BANNER('full_top'); ?> 

		<div class="row core_section_top_row">
 
<?php hook_breadcrumbs_before(); ?>

<?php echo hook_breadcrumbs(_design_breadcrumbs()); ?>

<?php hook_breadcrumbs_after(); ?>

<?php hook_core_columns_wrapper_inside(); ?>

	<?php if(!isset($GLOBALS['flag-custom-homepage'])): ?>
 
 	<?php hook_core_columns_wrapper_inside_inside(); ?>	
	
		<?php if(!isset($GLOBALS['nosidebar-left'])): ?>
        
        <?php get_template_part( 'sidebar', 'left' ); ?> 
      
        <?php endif; ?>    
 
	<article class="<?php $CORE->CSS("columns-middle"); ?>" id="core_middle_column"><?php echo $CORE->ERRORCLASS(); ?><div id="core_ajax_callback"></div><?php echo $CORE->BANNER('middle_top'); ?>
	
	<?php hook_core_columns_wrapper_middle_inside();  ?> 
       
	<?php endif; ?>